import org.json.simple.JSONArray
import org.json.simple.JSONObject
import org.json.simple.parser.JSONParser
import java.io.File
import java.io.FileReader
import java.io.FileWriter
import java.io.IOException
import java.lang.Exception
import java.lang.StringBuilder
import javax.script.ScriptEngineManager

enum class Editors(val data: EditorData) {
    DROP_TABLES(object : EditorData("drop_tables.json"){
        var counter = 0
        override fun parse() {
            Logger.logInfo("Parsing Drop Tables")
            configureParser()
            data.forEach { dropTableRaw ->
                val dropTable = dropTableRaw as org.json.simple.JSONObject
                val table = NPCDropTable()
                table.ids = dropTable["ids"].toString()
                parseTable(dropTable["main"] as JSONArray,table,false)
                parseTable(dropTable["default"] as JSONArray,table,true)
                parseTable(dropTable["charm"] as JSONArray,table.charmTable,false)
                table.description = (dropTable["description"] ?: "").toString()
                TableData.tables.add(table)
                counter++
            }
            Logger.logInfo("$counter Drop Tables Parsed.")
        }

        override fun save() {
            val CHARMS = intArrayOf(12160,12163,12159,12158)
            val dtables = JSONArray()
            for(table in TableData.tables){
                val t = JSONObject()
                t.put("ids",table.ids)
                val main = saveTable(table)
                val charms = saveTable(table.charmTable)
                val always = saveTable(table.alwaysTable)
                t.put("main",main)
                t.put("charm",charms)
                t.put("default",always)
                t.put("description",table.description)
                dtables.add(t)
            }


            val manager = ScriptEngineManager()
            val scriptEngine = manager.getEngineByName("JavaScript")
            scriptEngine.put("jsonString", dtables.toJSONString())
            scriptEngine.eval("result = JSON.stringify(JSON.parse(jsonString), null, 2)")
            val prettyPrintedJson = scriptEngine["result"] as String

            try {
                FileWriter(EditorConstants.CONFIG_PATH + File.separator + fileName).use { file ->
                    file.write(prettyPrintedJson)
                    file.flush()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }

        override fun show() {
            super.show()
        }
    }),
    NPC_CONFIGS(object : EditorData("npc_configs.json"){
        override fun parse() {
            Logger.logInfo("Parsing NPC configs")
            configureParser()
            data.forEach { npcDataRaw ->
                val npcData = npcDataRaw as JSONObject
                val id = npcData["id"].toString().toInt()
                val name = npcData["name"].toString()
                TableData.npcNames[id] = name
                TableData.npcConfigKeys.addAll(npcData.keys.toHashSet() as HashSet<String>)
                TableData.npcConfigs.add(npcData)
            }
        }

        override fun save() {
            val array = JSONArray()
            TableData.npcConfigs.forEach {
                array.add(it)
            }
            val manager = ScriptEngineManager()
            val scriptEngine = manager.getEngineByName("JavaScript")
            scriptEngine.put("jsonString", array.toJSONString())
            scriptEngine.eval("result = JSON.stringify(JSON.parse(jsonString), null, 2)")
            val prettyPrintedJson = scriptEngine["result"] as String

            try {
                FileWriter(EditorConstants.CONFIG_PATH + File.separator + fileName).use { file ->
                    file.write(prettyPrintedJson)
                    file.flush()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }),
    ITEM_CONFIGS(object : EditorData("item_configs.json"){
        override fun parse() {
            configureParser()
            data.forEach { itemDataRaw ->
                val itemData = itemDataRaw as JSONObject
                val id = itemData["id"].toString().toInt()
                val name = itemData["name"].toString()
                TableData.itemConfigKeys.addAll(itemData.keys.toHashSet() as HashSet<String>)
                TableData.itemNames[id] = name
                TableData.itemConfigs.add(itemData)
            }
        }

        override fun save() {
            val array = JSONArray()
            TableData.itemConfigs.forEach {
                array.add(it)
            }
            val manager = ScriptEngineManager()
            val scriptEngine = manager.getEngineByName("JavaScript")
            scriptEngine.put("jsonString", array.toJSONString())
            scriptEngine.eval("result = JSON.stringify(JSON.parse(jsonString), null, 2)")
            val prettyPrintedJson = scriptEngine["result"] as String

            try {
                FileWriter(EditorConstants.CONFIG_PATH + File.separator + fileName).use { file ->
                    file.write(prettyPrintedJson)
                    file.flush()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }),
    OBJECT_CONFIGS(object : EditorData("object_configs.json"){
        override fun parse() {
            Logger.logInfo("Parsing Object configs")
            configureParser()
            data.forEach { ObjDataRaw ->
                val objData = ObjDataRaw as JSONObject
                TableData.objConfigKeys.addAll(objData.keys.toHashSet() as HashSet<String>)
                TableData.objConfigs.add(objData)
            }
        }

        override fun save() {
            val array = JSONArray()
            TableData.objConfigs.forEach {
                array.add(it)
            }
            val manager = ScriptEngineManager()
            val scriptEngine = manager.getEngineByName("JavaScript")
            scriptEngine.put("jsonString", array.toJSONString())
            scriptEngine.eval("result = JSON.stringify(JSON.parse(jsonString), null, 2)")
            val prettyPrintedJson = scriptEngine["result"] as String

            try {
                FileWriter(EditorConstants.CONFIG_PATH + File.separator + fileName).use { file ->
                    file.write(prettyPrintedJson)
                    file.flush()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    }),
    SHOPS(object : EditorData("shops.json"){
        override fun parse() {
            Logger.logInfo("Parsing shop data...")
            configureParser()
            var counter = 0

            data.forEach { shopDataRaw ->
                val shopData = shopDataRaw as JSONObject
                val id = shopData["id"].toString().toInt()
                val title = shopData["title"].toString()
                val general = shopData["general_store"].toString().toBoolean()
                val stock = parseStock(shopData["stock"].toString())
                val npcs = if(shopData["npcs"].toString().isNotBlank()) shopData["npcs"].toString() else ""
                val currency = shopData["currency"].toString().toInt()
                val highAlch = shopData["high_alch"].toString() == "1"
                TableData.shops[id] = TableData.Shop(id,title,stock,npcs,currency,general,highAlch)
                counter++
            }

            Logger.logInfo("Loaded $counter shops.")
        }

        override fun save() {
            val shs = JSONArray()

            fun <T> ArrayList<T>.isLast(thing: T): Boolean{
                return indexOf(thing) == size - 1
            }

            for((_,shop) in TableData.shops){
                val sh = JSONObject()
                sh.put("id",shop.id.toString())
                sh.put("title",shop.title)
                val stockBuilder = StringBuilder()
                for(item in shop.stock){
                    stockBuilder.append("{${item.id},${if(item.infinite) "inf" else item.amount}}${if(shop.stock.isLast(item)) "" else "-"}")
                }
                sh.put("stock",stockBuilder.toString())
                sh.put("npcs",shop.npcs)
                sh.put("currency",shop.currency.toString())
                sh.put("high_alch",if(shop.high_alch) "1" else "0")
                sh.put("general_store",shop.general_store.toString())
                shs.add(sh)
            }


            val manager = ScriptEngineManager()
            val scriptEngine = manager.getEngineByName("JavaScript")
            scriptEngine.put("jsonString", shs.toJSONString())
            scriptEngine.eval("result = JSON.stringify(JSON.parse(jsonString), null, 2)")
            val prettyPrintedJson = scriptEngine["result"] as String

            try {
                FileWriter(EditorConstants.CONFIG_PATH + File.separator + fileName).use { file ->
                    file.write(prettyPrintedJson)
                    file.flush()
                }
            } catch (e: IOException) {
                e.printStackTrace()
            }
        }
    })
}


fun parseStock(stock: String): ArrayList<Item>{
    val items = ArrayList<Item>()
    if(stock.isEmpty()){
        return items
    }
    stock.split('-').map {
        val tokens = it.replace("{", "").replace("}", "").split(",".toRegex()).toTypedArray()
        var amount = tokens[1].trim()
        items.add(Item(tokens[0].trim().toInt(),amount,amount == "inf"))
    }
    return items
}
private fun parseTable(data: JSONArray, destTable: WeightBasedTable, isAlways: Boolean) {
    for(it in data){
        val item = it as JSONObject
        val id = item["id"].toString().toInt()
        val minAmount = item["minAmount"].toString().toInt()
        val maxAmount = item["maxAmount"].toString().toInt()
        val weight = item["weight"].toString().toDouble()
        val newItem = WeightedItem(id,minAmount.toString(),maxAmount.toString(),weight.toDouble(),isAlways)
        destTable.add(newItem)
    }
}
private fun saveTable(table: WeightBasedTable): JSONArray{
    val arr = JSONArray()
    for(item in table){
        val it = JSONObject()
        it.put("id",item.id.toString())
        it.put("weight",item.weight.toString())
        it.put("minAmount",item.minAmt.toString())
        it.put("maxAmount",item.maxAmt.toString())
        arr.add(it)
    }
    return arr
}
